export JAVA_HOME=/opt/jdk
export ANT_HOME=/opt/ant
export MAVEN_HOME=/opt/maven
export SCALA_HOME=/opt/scala
export SBT_HOME=/opt/sbt

export IDEA_HOME=/home/$(whoami)/tools/idea
export IDEA_JDK=/opt/jdk6

export PATH=$MAVEN_HOME/bin:$SBT_HOME:$SCALA_HOME/bin:$ANT_HOME/bin:$JAVA_HOME/bin:$PATH
