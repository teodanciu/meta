export PATH=/usr/extra/bin:$PATH
export EDITOR=sublime-text
export ME=$(whoami)
export PROMPT_COMMAND='history -a'
