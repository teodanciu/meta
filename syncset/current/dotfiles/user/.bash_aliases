alias with='xargs'
alias we='xargs $EDITOR'
alias e='$EDITOR'
alias log='gnome-system-log'
alias x='xsel -b'

alias lsa='ls -a' 
alias less='less -R'

alias play='/opt/play/play'

alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -la --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias grep='grep --color=tty -d skip'
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

