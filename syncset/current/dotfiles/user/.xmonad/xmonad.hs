import XMonad
import XMonad.Config.Xfce
import XMonad.Hooks.SetWMName
import qualified Data.Map as M
import Graphics.X11.Xlib
import Data.Ratio

import XMonad.Config.Xfce
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ICCCMFocus
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.NoBorders
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Prompt.XMonad
import qualified XMonad.StackSet as W

main = xmonad xfceConfig {
  modMask = mod4Mask,
  terminal = "xfce4-terminal",
  manageHook = myManageHook,
  startupHook = setWMName "LG3D",
  layoutHook = smartBorders $ layoutHook xfceConfig,
  keys = myKeys
}

myKeys x = M.union (keys defaultConfig x) (M.fromList (myKeysConfig x))
myKeysConfig conf@(XConfig {
    XMonad.modMask = mod4Mask
  }) = [
     -- launch dmenu
     ((mod4Mask, xK_p), spawn "dmenu_run")
     -- ((mod4Mask, xK_F12), xmonadPrompt defaultXPConfig),
     -- ((mod4Mask, xK_F3 ), shellPrompt  defaultXPConfig)
  ]
  ++
-- mod-shift-[1..9] %! Move client to workspace N
    [((m .|. mod4Mask, k), windows $ f i)
      | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
      , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
  ++
  -- mod-{w,e,r} %! Switch to physical/Xinerama screens 1, 2, or 3
  -- mod-shift-{w,e,r} %! Move client to screen 1, 2, or 3
  [((m .|. mod4Mask, key), screenWorkspace sc >>= flip whenJust (windows . f))
      | (key, sc) <- zip [xK_w, xK_e, xK_o] [0..]
      , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myManageHook = composeAll
  [manageHook xfceConfig,
  --isFullscreen --> doFullFloat,
  title =? "Application Finder" --> doCenterFloat	 
  ]
