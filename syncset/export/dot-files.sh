#!/bin/bash

function bashrc() {
	copy "$homedir/.git-prompt.sh" "$outputdir"
	copy "$homedir/.bashrc" "$outputdir"
}

function inputrc() {
	copy "$homedir/.inputrc" "$outputdir"
}

function bash_aliases() {
	copy "$homedir/.bash_aliases" "$outputdir"
}

function aliases() {
	bash_aliases
}
 
function dircolors() {
	copy "$homedir/.dircolors" "$outputdir"
}

function gnome-terminal() {
	copy "$homedir/.gconf/apps/gnome-terminal" "$outputdir/.gconf/apps"
}

function terminal() {
	gnome-terminal
}

function vim() {
	copy "$homedir/.vim" "$outputdir"
}

function git() {
	copy "$homedir/.gitconfig" "$outputdir"
}

function xmonad() {
	copy "$homedir/.xmonad" "$outputdir"
}

source ../common.sh
source dot-files.properties

homedir=/home/$ME
outputdir=$CURRENT/dotfiles/user
mkdir -p $outputdir

read -a settings <<< "${@:-${what}}"

spacer
info "exporting system settings to $outputdir of size $(size $outputdir)..."
spacer

for setting in "${settings[@]}"; do
	spacer "$line"
	spacer " $setting"
	spacer "$line"	
	$setting	
	spacer
done

spacer
info "exported system settings to $outputdir of size $(size $outputdir)."




