#!/bin/bash

function git() {
	copy "$homedir/.gitconfig" "$outputdir"
}

function maven() {
	mavendir="$homedir/.m2"
	files=$(find $mavendir -maxdepth 1 -name ".settings-ORIGINAL-*.xml*" -or -name "settings.xml" -or -name "settings")
	for file in $files; do
		copy "$file" "$outputdir/.m2" 
	done
}

function sublime-text() {
	copy "$homedir/.config/sublime-text-2/Packages/User" "$outputdir/.config/sublime-text-2/Packages"
}

function sublime() {
	sublime-text
}

function idea() {
	for i in $(find $homedir -maxdepth 1 -name ".IntelliJ*"); do
		configdir=$(basename $i)
		copy "$i/config/codestyles" "$outputdir/$configdir/config"
		copy "$i/config/colors" "$outputdir/$configdir/config"
		copy "$i/config/disabled_plugins.txt" "$outputdir/$configdir/config"
		copy "$i/config/fileTemplates" "$outputdir/$configdir/config"
		copy "$i/config/filetypes" "$outputdir/$configdir/config"
		copy "$i/config/inspection" "$outputdir/$configdir/config"
		copy "$i/config/keymaps" "$outputdir/$configdir/config"
		copy "$i/config/options" "$outputdir/$configdir/config"
		copy "$i/config/templates" "$outputdir/$configdir/config"
		copy "$i/config/tools" "$outputdir/$configdir/config"
	done

	copy $IDEA_HOME/settings*.jar "$outputdir/tools/idea"
}

source ../common.sh
source tools-settings.properties

homedir=/home/$ME
outputdir=$CURRENT/toolssettings/user
mkdir -p $outputdir

read -a settings <<< "${@:-${what}}"

spacer
info "exporting settings to $outputdir of size $(size $outputdir)..."
spacer

for setting in "${settings[@]}"; do
	spacer "$line"
	spacer " $setting"
	spacer "$line"	
	$setting	
	spacer
done

spacer
info "exported settings to $outputdir of size $(size $outputdir)."




