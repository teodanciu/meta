#!/bin/bash

source ../common.sh

outputdir=$CURRENT/etc/profile.d
mkdir -p $outputdir

spacer
info "exporting /etc/profile.d of size $(size /etc/profile.d) to $outputdir of size $(size $outputdir)"
spacer

if [ $# -gt 0 ]; then
	params=$(echo $@ | awk 'BEGIN { RS = " "; ORS = " "}; {print "/etc/profile.d/"$0;}')
fi
etclist=$(echo $(ls /etc/profile.d/*.sh))
read -a scripts <<< "${params:-${etclist}}"

for script in "${scripts[@]}"; do
	copy "$script" "$outputdir"
done

spacer
info "exported to $outputdir of size $(size $outputdir)."


