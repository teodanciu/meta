line="========================================================================="

red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
reset=$(tput sgr0) 

function debug() {
	echo -e "${blue} DEBUG: $1${reset}"	
}

function info() {
	echo "${green} INFO: $1${reset}"	
}

function warn() {
	echo "${yellow} WARN: $1${reset}"
}

function error() {
	echo "${red} ERROR: $1${reset}"
}

function spacer() {
	echo $1
}

function size() {
	du -sh $1 | cut -f1
}

function copy() {
	mkdir -p "$2"
	
	file=$(basename $1)
	cp -R "$1" "$2"
	if [ $? != 0 ]; then
		error "could NOT copy $1 ($(size $1))!"
	else
		info "copied $1 ($(size $1)) - ($(size $2/$file)) to $2"
	fi
}

function backup() {
	echo "${blue}"
	$META_HOME/commands/bck $1
	echo "${reset}"
}

function saywhat() {
	if [ $# -eq 1 ] && [ $1 == "what" ]; then
  		echo ${what}
  		if [ ! -z "$extra" ]; then 
  			echo "extra: $extra";
  		fi
  		exit
	fi
}

ME="teo"
META_HOME=/home/$ME/meta
CURRENT=$META_HOME/syncset/current