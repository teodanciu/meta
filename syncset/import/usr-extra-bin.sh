#!/bin/bash

source ../common.sh

inputdir=$META_HOME/commands
outputdir=/usr/extra/bin

if [ $# -eq 1 ] && [ $1 == "what" ]; then
  	ls $inputdir
  	exit
fi

if [ ! -d `dirname $outputdir` ]; then
	warn "`dirname $outputdir` does not exist, creating."
	mkdir -p `dirname $outputdir`
fi

function checksymlink() {
	target=$1
	source=$2
	
	if [[ ( -L $target ) ]]; then
			if [ ! -e $target ]; then
				error "created dangling symlink $target for $source!"
			else
				info "created valid symlink $target for $source."
			fi
	else
		error "could NOT create symlink $target for $source!"
	fi
}

debug "ln -s $inputdir $outputdir"
ln -s $inputdir $outputdir

checksymlink $outputdir $inputdir
