#!/bin/bash

function get() {
	spacer
	filename=$(basename "$url")

	info "trying to download $filename to $downloaddir from $url..."

	if [ ! -d $downloaddir ]; then
		warn "$downloaddir does not exist, creating."
		mkdir -p $downloaddir
	fi

	if [ -f $downloaddir/$filename ]; then
		warn  "$downloaddir/$filename already exists. Not downloading."
		return
	fi
	eval "wget $params $url -P $downloaddir"
	if [ ! -f $downloaddir/$filename ]; then
		error "could NOT download from $url to $downloaddir! "
		return
	else 
		info "downloaded $filename of `size $downloaddir/$filename` to $downloaddir."
	fi
}

function install() {
	spacer
	archive="$downloaddir/$filename"
	info "trying to unarchive $archive of size `size $archive` to $distdir..."
	
	if [ ! -d $distdir ]; then
		warn "$distdir does not exist, creating."
		mkdir -p $distdir
	fi

	bluff=false
	before=`ls -1 $distdir`
	if [ ! -f "$archive" ]; then
		bluff=true
	elif [[ $archive == *.tar.gz  || $archive == *.tgz ]]; then
		tar xvzf "$archive" -C $distdir > /dev/null
		[[ $? != 0 ]] && bluff=true
	elif [[ $archive == *.tar.bz2 ]]; then
		tar xvjf "$archive" -C $distdir
		[[ $? != 0 ]] && bluff=true
	elif [[ $archive == *.zip ]] ; then
		unzip "$archive" -d $distdir
		[[ $? != 0 ]] && bluff=true
	else 
		bluff=true
	fi

	after=`ls -1 $distdir`
	unarchive=`comm -1 -3 <(echo "$before") <(echo "$after")`
	if $bluff; then
		error "could NOT unarchive: $archive of size `size $archive` to $distdir!. Trying to just copy."
		mkdir -p $distdir/$filename
		cp -r "$archive" "$distdir"/"$filename"
		if [[ $? != 0 ]]; then
			error "copying failed as well!"	
	  	else
			info "copied, but not unarchived $archive to $distdir/$filename, size `size $distdir/$filename/$filename`"
			bluff=false
		fi
	else
		info "unarchived $archive to $distdir, probably as: $unarchive of size `size $distdir/$unarchive`."
	fi
}


function permissions() {
	if $bluff; then
		return
	fi
	spacer
	if [ -d "$distdir/$unarchive/bin" ]; then
		target="$distdir/$unarchive/bin"		
	else
		target="$distdir/$unarchive"
	fi
	permissions=`stat -c %a $target`
	chmod -R 775 $target
	if [[ $? != 0 ]]; then
		error "could NOT update permissions on $target from $permissions to 755!"
	else
		info "updated permissions on $target from $permissions to 755."
	fi

	spacer
	owner=`stat -c %U $archive`
	chown -R $ME $archive 
	if [[ $? != 0 ]]; then
		error "could NOT change owner of $archive from $owner to $ME!"
	else
		info "changed owner of $archive from $owner to $ME."
	fi
}

function symlink() {
	if $bluff; then
		return
	fi
	spacer

	if [ ! -z $extrasymlink ]; then

		if [ -e $distdir/extrasymlink ]; then
			warn "$distdir/$extrasymlink already exists. Could not create symlink with the same name."
			return
		fi

		debug "ln -s $unarchive $distdir/$extrasymlink"
		ln -s $unarchive $distdir/$extrasymlink
		checksymlink $extrasymlink $unarchive
	else 
		extrasymlink=$unarchive
	fi
	if  ! $bluff ; then
		if [ -e $distdir/$dist ]; then
			warn "$distdir/$dist already exists. Could not create symlink with the same name."
			return
		fi

		debug "ln -s $extrasymlink $distdir/$dist"
		ln -s $extrasymlink $distdir/$dist
		checksymlink $dist $extrasymlink
	fi
	unset extrasymlink
}

function checksymlink() {
	target=$1
	source=$2
	bluff=false
	
	if [[ ( -L $distdir/$target ) ]]; then
			if [ ! -e $distdir/$target ]; then
				error "created dangling symlink $target for $distdir/$source!"
				bluff=true
			else
				info "created valid symlink $target for $distdir/$source."
			fi
	else
		error "could NOT create symlink $target for $distdir/$source!"
		bluff=true
	fi
}

source ../common.sh
source opt.properties

saywhat $@

read -a dists <<< "${@:-${what}}"

for dist in "${dists[@]}"
do
	spacer; spacer
	spacer "$line"
	eval $dist
	spacer " $dist $version"
	spacer "$line"
	filename=$(basename "$url")
	get
	install
	permissions
	symlink
done
