#!/bin/bash

function bashrc() {
	backup "$outputdir/.bashrc"
	copy "$inputdir/.git-prompt.sh" "$outputdir"
	copy "$inputdir/.bashrc" "$outputdir"
}

function inputrc() {
	backup "$outputdir/.inputrc"
	copy "$inputdir/.inputrc" "$outputdir"
}

function bash_aliases() {
	backup "$outputdir/.bash_aliases"
	copy "$inputdir/.bash_aliases" "$outputdir"
}

function aliases() {
	bash_aliases
}
 
function dircolors() {
	backup "$outputdir/.dircolors"
	copy "$inputdir/.dircolors" "$outputdir"
}

function gnome-terminal() {
	copy "$inputdir/.gconf/apps/gnome-terminal" "$outputdir/.gconf/apps"
}

function terminal() {
	gnome-terminal
}

function vim() {
	copy "$inputdir/.vim" "$outputdir"
	ln -s "$homedir/.vim/vimrc" "$homedir/.vimrc"
}

function git() {
	backup "$outputdir/.gitconfig"
	copy "$inputdir/.gitconfig" "$outputdir"
}

function xmonad() {
	backup "$outputdir/.xmonad"
	copy "$inputdir/.xmonad/xmonad.hs" "$outputdir/.xmonad"
}


source ../common.sh
source dot-files.properties

saywhat $@

homedir=/home/$ME
inputdir=$CURRENT/dotfiles/user
outputdir=$homedir

read -a settings <<< "${@:-${what}}"

spacer
info "importing system settings from $inputdir of size $(size $inputdir)..."
spacer

backup "$inputdir/.gconf"
backup "$inputdir/.config"

for setting in "${settings[@]}"; do
	spacer "$line"
	spacer " $setting"
	spacer "$line"	
	$setting	
	spacer
done

spacer
info "imported system settings from $inputdir."




