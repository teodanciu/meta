#!/bin/bash

function repos() {
	spacer
	info "enabling additional repos..."
	cp /etc/apt/sources.list /etc/apt/sources.list.backup
	sed -i -e "/deb cdrom/b; s/# deb/deb/g" /etc/apt/sources.list
	if [ $? != 0 ]; then
		warn "could NOT enable repos, trying to restore backed-up sources.list.backup"
		cp /etc/apt/sources.list.backup /etc/apt/sources.list
		
		if [ $? != 0 ]; then
			error "something went really wrong! " 
		fi
	else
		info "additional repos enabled."
	fi
}

function ppas() {
	spacer
	info "installing additional ppa repositories.."
	for pparepo in $ppas; do 
		spacer "$line"
		spacer "$pparepo"
		spacer "$line"
		add-apt-repository -y "ppa:$pparepo"
		if [ $? != 0 ]; then
			error "could not add $pparepo!"
		else
			info "probably success."
		fi
	done
	spacer
	info "updating..."
	apt-get update
}

function installpackages() {
	spacer
	info "installing packages..."
	read -a packages <<< "${@:-${what}}"
	for package in "${packages[@]}"; do
		spacer; spacer "$line"
		spacer " $package"
		spacer "$line"	
		apt-get -y install  $package
		if [ $? != 0 ]; then
			error "could not install $package!"
		else
			info "installed $package (or it was already installed)."
		fi
	done
}


source ../common.sh
source packages.properties

saywhat $@

repos
ppas
installpackages "$@"