#!/bin/bash

source ../common.sh

if [ $# -eq 1 ] && [ $1 == "what" ]; then
  	ls  $CURRENT/etc/profile.d
  	exit
fi

inputdir=$CURRENT/etc/profile.d
outputdir="/etc/profile.d"

backup "$outputdir"

if [ ! -d $outputdir ]; then
	warn "/etc/profile.d does not exist, creating."
	mkdir -p $outputdir
fi

spacer
info "importing $inputdir of size $(size $inputdir) to /etc/profile.d of size $(size $outputdir)"
spacer

if [ $# -gt 0 ]; then
	params=$(echo $@ | awk -v input=$inputdir 'BEGIN { RS = " "; ORS = " "}; {print input "/" $0;}')
fi

etclist=$(echo $(ls $inputdir/*.sh))
read -a scripts <<< "${params:-${etclist}}"

for script in "${scripts[@]}"; do
	copy "$script" "$outputdir"
	source "$script"
done

spacer
info "exported to /etc/profile.d of size $(size $outputdir)."

