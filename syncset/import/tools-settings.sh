#!/bin/bash

function maven() {
	mavendir="$inputdir/.m2"
	files=$(find $mavendir -maxdepth 1 -name ".settings-ORIGINAL-*.xml*" -or -name "settings.xml" -or -name "settings")
	for file in $files; do
		backup "$outputdir/.m2/$(basename $file)"
		copy "$file" "$outputdir/.m2" 
	done
}

function sublime-text() {
	backup "$outputdir/.config/sublime-text-2"
	copy "$inputdir/.config/sublime-text-2" "$outputdir/.config"
}

function sublime() {
	sublime-text
}

function idea() {
	for i in $(find $inputdir -maxdepth 1 -name ".IntelliJ*"); do
		backup "$outputdir/$(basename $i)"
		copy "$i" "$outputdir"
	done
	
	mkdir -p $homedir/tools/idea
	copy $inputdir/tools/idea/*.jar $homedir/$ME/tools/idea
}

source ../common.sh
source tools-settings.properties

saywhat $@

homedir=/home/$ME
inputdir=$CURRENT/toolssettings/user
outputdir=$homedir

read -a settings <<< "${@:-${what}}"

spacer
info "importing settings from $inputdir of size $(size $inputdir)..."
spacer

for setting in "${settings[@]}"; do
	spacer "$line"
	spacer " $setting"
	spacer "$line"	
	$setting	
	spacer
done

spacer
info "imported settings from $inputdir"




