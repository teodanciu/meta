#!/bin/bash
source ../common.sh
source dirs-setup.properties

saywhat $@

spacer

homedir=/home/$ME
info "removing default folders..."
read -a dirs_to_remove <<< "${@:-${what}}"
for dir in "${dirs_to_remove[@]}"; do
	spacer; spacer "$line"
	spacer " $dir"
	spacer "$line"	
	rm -rf $homedir/$dir
	if [ $? != 0 ]; then
		error "could not remove $dir!"
	else
		info "removed $dir (or wasn't there)."
	fi
done

spacer
info "creating folders..."
read -a dirs_to_create <<< "${@:-${what_to_create}}"
for dir in "${dirs_to_create[@]}"; do
	spacer; spacer "$line"
	spacer " $dir"
	spacer "$line"
	if [[ "$dir" =~ ^/.* ]]; then	
		tocreate=$dir
	else
		tocreate=$homedir/$dir
	fi
	mkdir -p $tocreate
	chown $ME $tocreate
	if [ $? != 0 ]; then
		error "could not create $tocreate!"
	else
		info "created $tocreate (or was already there)."
	fi
done
